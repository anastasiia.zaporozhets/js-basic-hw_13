'use strict'

const tabs = document.querySelector(".tabs");
const tabTitles = document.querySelectorAll(".tabs-title");
const tabContents = document.querySelectorAll(".tabs-content > li");


for (let i = 1; i < tabContents.length; i++) {
    tabContents[i].classList.add("active-none");
}

tabContents[0].classList.add("active");


tabs.addEventListener("click", (event) => {
    event.preventDefault();

    if (event.target.classList.contains("tabs-title")){
        tabTitles.forEach(tabTitle =>{
            tabTitle.classList.remove("active");
        })
        event.target.classList.add("active");
    }

    const arrayTabTitles = [...tabTitles];
    const arrayTabTitlesIndex = arrayTabTitles.indexOf(event.target);

    tabContents.forEach((element, index)=>{
        if (index === arrayTabTitlesIndex){
            element.classList.remove("active-none");
            element.classList.add("active")
        } else {
            element.classList.add("active-none")
        }
    })
});




